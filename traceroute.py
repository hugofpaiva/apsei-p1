import os
import time
import random
import datetime
import ipaddress
import argparse
import plotly.graph_objects as go
import plotly.express as px

all_outputs = []
hops = {}
counter_trace = 0

def check_ipv4(ip):
    try:
        ipaddress.IPv4Network(ip)
        return True
    except ValueError:
        return False

def has_numbers(string):
    return any(char.isdigit() for char in string)

def save_in_hops(ctr_hops, address):
    if address in hops[ctr_hops].keys():
        hops[ctr_hops][address] += 1
    else:
        hops[ctr_hops][address] = 1

def make_connections_graph():
    data = []

    colors = px.colors.qualitative.Alphabet

    address_colors = {}

    asterix_hops_recurrence = ([], [])

    counter_colors = 0

    for hop in hops.keys():
        for address in hops[hop].keys():
            if address not in address_colors.keys():
                address_colors[address] = colors[counter_colors]
                if counter_colors == len(px.colors.qualitative.Dark24)-1:
                    counter_colors = 0
                else:
                    counter_colors += 1
            if address == '*':
                asterix_hops_recurrence[0].append(hop)
                asterix_hops_recurrence[1].append(hops[hop][address])
            else:
                data.append(go.Bar(name=address, x=[hop], y=[hops[hop][address]], marker_color=address_colors[address]))

    data.append(go.Bar(
        name='*', x=asterix_hops_recurrence[0], y=asterix_hops_recurrence[1], marker_color=address_colors['*']))

    fig = go.Figure(data=data)
    # Change the bar mode
    fig.update_layout(barmode='stack', xaxis=dict(
        title='Hop number',
        tickmode='linear'), yaxis=dict(title='Number of connections to hostnames'))

    fig.write_image("connections.png", width=1980, height=1080)

def main(repetitions):
    global all_outputs
    global hops
    global counter_trace

    while counter_trace < repetitions:
        request_time = datetime.datetime.now().strftime("%Y/%m/%d, %H:%M:%S")
        stream = os.popen('traceroute www.cmu.edu')
        output = stream.readlines()

        all_outputs.append((request_time, output))

        print("Sleeping")
        time.sleep(random.randint(300, 600))
        counter_trace += 1

    f = open("traceroutes.txt", "w")
    for output in all_outputs:
        f.write("\n"+output[0]+"\n")
        counter_hops = 0
        for line in output[1]:
            if has_numbers(line[:4]):
                counter_hops += 1

            if counter_hops not in hops.keys():
                hops[counter_hops] = {}

            if '* * *' in line[4:]:
                save_in_hops(counter_hops, '*')
                save_in_hops(counter_hops, '*')
                save_in_hops(counter_hops, '*')
            else:
                splited_line = line[4:].split(' ')
                for idx, word in enumerate(splited_line):
                    if '*' in word:
                        save_in_hops(counter_hops, '*')
                    elif idx < len(splited_line)-1 and check_ipv4(splited_line[idx+1][1:-1]):
                        splited_line[idx+1][1:-1]
                        index = idx + 2
                        while index <= len(splited_line)-1:
                            if 'ms' in splited_line[index]:
                                save_in_hops(counter_hops, word)
                            index+=1
            
            f.write(line)
    f.close()

    make_connections_graph()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r','--repetitions', help='Number of traceroutes to make', default=1, type=int)
    args = parser.parse_args()
    main(args.repetitions)