# Anatomy of a Web Connection: A Brief Analysis

The rapid evolution of the Internet has masked the average user for the real complexity that makes up its operation. Despite the impact on society, making it dependent on the same daily rate, a large part of the population does not understand how, for example, their devices access a website or communicate with other data elsewhere on the planet. This document aims to analyze the anatomy of an Internet connection, examining the path taken to reach its goal, identifying how technologies and processes are required, and studying the socio-economic impacts caused in society.

## Auxiliar Python Script

In order to make an automatic test to see the differences in the _hops_ of the _traceroute_ command to the _www.cmu.edu_ server, it was created a script in _Python_ to make **n** _traceroutes_ with 5-10 minute interval between them. At the end, the program generates a chart to make easier the analysis of the results.

### How to install

Make sure you are running Python 3.5 or higher

1. Create a virtual environment (venv)
```bash
python3 -m venv venv
```

2. Activate the virtual environment (you need to repeat this step, and this step only, every time you start a new terminal/session):
```bash
source venv/bin/activate
```

3. Install the game requirements:
```bash
pip install -r requirements.txt
```

### Running

**Options:**

```
python3 traceroute.py -r <repetitions>.......Number of traceroutes to make
```
